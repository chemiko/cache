package com.company;

import com.company.cached.Cache;
import com.company.items.IEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 * Created by me on 17.03.2016.
 */
public class ItemFloatIRepo implements IRepo<Float,IEntity<Float>> {
    private Map<Float,IEntity<Float>> repo;

    public ItemFloatIRepo(){
        Cache cache = Cache.getInstance();
        //repo = cache.getRepo(new Float(1));
    }


    @Override
    public IEntity<Float> save(IEntity<Float> item) {
        repo.put(item.getId(),item);
        return item;
    }

    @Override
    public IEntity<Float> removeById(Float id) {
        if(repo.containsKey(id)){
            IEntity<Float> entity = repo.remove(id);
            return entity;
        }
        return null;
    }

    @Override
    public IEntity<Float> remove(IEntity<Float> item){
        return removeById(item.getId());
    }


    @Override
    public List<IEntity<Float>> getItems() {
        List<IEntity<Float>> coll = (List<IEntity<Float>>) repo.values();

        return coll;
    }

    @Override
    public IEntity<Float> getByID(Float id) {
        if(repo.containsKey(id)){
            IEntity<Float> entity = repo.get(id);
            return entity;
        }
        return null;
    }

    @Override
    public List<IEntity<Float>> filter(Predicate<IEntity<Float>> predicate) {
        List<IEntity<Float>> col = new ArrayList<>();
        for(IEntity<Float> e : repo.values()){
            if(predicate.test(e)){
                col.add(e);
            }
        }
        if(col.isEmpty()){
            return null;
        }
        return col;
    }
}
