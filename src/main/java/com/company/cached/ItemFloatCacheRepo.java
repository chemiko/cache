package com.company.cached;

import com.company.IRepo;
import com.company.cached.Cache;
import com.company.items.ItemFloat;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by me on 03.04.2016.
 */
public class ItemFloatCacheRepo implements IRepo<Float, ItemFloat> {
    private Cache cache = null;

    public ItemFloatCacheRepo() {
        this.cache = Cache.getInstance();
    }

    @Override
    public ItemFloat save(ItemFloat item) {
        cache.putEntity(ItemFloat.class, item);
        return null;
    }

    @Override
    public ItemFloat removeById(Float id) {
        ItemFloat i = cache.removeEntityById(ItemFloat.class, id);
        return i;
    }

    @Override
    public ItemFloat remove(ItemFloat item) {
        return removeById(item.getId());
    }

    @Override
    public List<ItemFloat> getItems() {
        return cache.getEntityes(ItemFloat.class);
    }

    @Override
    public ItemFloat getByID(Float id) {
        ItemFloat i = null;
        i = cache.getEntityById(ItemFloat.class, id);
        return i;
    }

    @Override
    public List<ItemFloat> filter(Predicate<ItemFloat> predicate) {
        List<ItemFloat> list = new ArrayList<>();
        for(ItemFloat i: getItems()){
            if(predicate.test(i)){
                list.add(i);
            }
        }
        return list;
    }
}
