package com.company.cached;

import com.company.items.IEntity;

import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by me on 17.03.2016.
 */
public class Cache {
    Map<Class<?>,Map> m = new HashMap<>();
    private static Cache cacheInstance = null;
    private ReentrantReadWriteLock rw = new ReentrantReadWriteLock();
    private Lock readLock = rw.readLock();
    private Lock writeLock = rw.writeLock();

    private static boolean isLogged = false;

    public static void setIsLogged(boolean b){
        isLogged = b;
    }

    private Cache(){

    }

    public static synchronized Cache getInstance(){
        if(cacheInstance == null ){
            cacheInstance = new Cache();
        }
        return cacheInstance;
    }

//    public <KEY,E extends IEntity> Map<KEY,E> getRepo(KEY k){
//        if(!m.containsKey(k.getClass())){
//            m.put(k.getClass(), new HashMap<KEY,Map<KEY,E>>());
//        }
//        return m.get(k.getClass());
//    }

    public <E extends IEntity,ID> E getEntityById(Class<?> classType, ID id){
        E entity = null;
        readLock.lock();
        try{
            if(!m.containsKey(classType)){
                m.put(classType, new HashMap<ID,E>());
            }
            Map<ID,E> temp = m.get(classType);
            entity = temp.get(id);
            if(isLogged) System.out.println("Read: " + entity);
        }finally {
            readLock.unlock();
        }
        return entity;
    }

    public <E extends IEntity, ID> E putEntity(Class<?> classType, E entity){
        writeLock.lock();
        try {
            if(!m.containsKey(classType)){
                m.put(classType, new HashMap<ID,E>());
            }
            Map<ID,E> temp = m.get(classType);
            temp.put((ID) entity.getId(),entity);
            if(isLogged) System.out.println("Write: " +  entity);
        }finally {
            writeLock.unlock();
        }
        return entity;

    }

    public <ID, E extends IEntity> E removeEntityById(Class<?> classType, ID id){

        E e = null;
        writeLock.lock();
        try{
            Map<ID, E> temp = m.get(classType);
            if(temp != null && temp.containsKey(id)){
                e = temp.remove(id);
                if(isLogged) System.out.println("delete: " + e);
            }
        }finally {
            writeLock.unlock();
        }
        if(e != null){
            return e;
        }else{
            return null;
        }
    }

    public <E extends IEntity> List<E> getEntityes(Class<?> classType){
        List<E> list = new ArrayList<>();
        readLock.lock();
        try{
            if(m.containsKey(classType)){
                Iterator<E> iter = m.get(classType).entrySet().iterator();
                while(iter.hasNext()){
                    Map.Entry<?,E> entry = (Map.Entry<?, E>) iter.next();
                    list.add(entry.getValue());
                }

            }
        }finally {
            readLock.unlock();
        }
        return list;
    }


}
