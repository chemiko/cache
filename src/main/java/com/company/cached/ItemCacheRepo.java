package com.company.cached;

import com.company.IRepo;
import com.company.cached.Cache;
import com.company.items.Item;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by me on 03.04.2016.
 */
public class ItemCacheRepo implements IRepo<Long, Item> {
    private Cache cache = null;

    public ItemCacheRepo() {
        this.cache = Cache.getInstance();
    }

    @Override
    public Item save(Item item) {
        cache.putEntity(Item.class, item);

        return null;
    }

    @Override
    public Item removeById(Long id) {
        Item i = cache.removeEntityById(Item.class, id);
        return i;
    }

    @Override
    public Item remove(Item item) {
        return removeById(item.getId());
    }

    @Override
    public List<Item> getItems() {
        return cache.getEntityes(Item.class);
    }

    @Override
    public Item getByID(Long id) {
        Item i = null;
        i = cache.getEntityById(Item.class, id);

        return i;
    }

    @Override
    public List<Item> filter(Predicate<Item> predicate) {
        List<Item> list = new ArrayList<>();
        for(Item i: getItems()){
            if(predicate.test(i)){
                list.add(i);
            }
        }
        return list;
    }
}
