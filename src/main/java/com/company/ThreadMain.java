package com.company;

import com.company.cached.Cache;
import com.company.threads.CacheReadTask;
import com.company.threads.CacheRemoveTask;
import com.company.threads.CacheWriteTask;

/**
 * Created by me on 04.04.2016.
 */
public class ThreadMain {
    public static void main(String[] args){
        Cache.setIsLogged(true);

        Thread t = new Thread(new CacheWriteTask(300L));
        Thread t2 = new Thread(new CacheWriteTask(200L));
        Thread t3 = new Thread(new CacheWriteTask(700L));
        t.start();
        t2.start();
        t3.start();
        Thread tRead = new Thread(new CacheReadTask(100L));
        Thread tRead2 = new Thread(new CacheReadTask(1000L));
        Thread tRead3 = new Thread(new CacheReadTask(600L));
        tRead.start();
        tRead2.start();
        tRead3.start();

        Thread tRemove = new Thread(new CacheRemoveTask(100L));
        tRemove.start();

    }
}
