package com.company;

import com.company.items.IEntity;

import java.util.Collection;
import java.util.List;
import java.util.function.Predicate;

/**
 * Created by me on 17.03.2016.
 */
public interface IRepo<ID,E extends IEntity> {
    E save(E item);
    E removeById(ID id);
    E remove(E item);
    List<E> getItems();
    E getByID(ID id);
    List<E> filter(Predicate<E> func);
}
