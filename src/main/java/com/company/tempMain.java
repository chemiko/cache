package com.company;

import com.company.cached.ItemCacheRepo;
import com.company.cached.ItemFloatCacheRepo;
import com.company.items.Item;
import com.company.items.ItemFloat;

import java.util.function.Predicate;

/**
 * Created by me on 03.04.2016.
 */
public class tempMain {

    public static void main(String[] args){

        Item one = new Item("one", 12.5, 10);
        ItemFloat floatOne = new ItemFloat("float one", 14.2, 20);
        ItemFloat floattwo = new ItemFloat("float two", 12.2, 40);
        ItemFloat floatthre = new ItemFloat("float three", 13.2, 10);
        ItemFloat floatfour = new ItemFloat("float four", 12.2, 30);

        IRepo<Long,Item> itemCacheRepo = new ItemCacheRepo();
        IRepo<Float,ItemFloat> itemFloatCacheRepo = new ItemFloatCacheRepo();

        System.out.print("Created item: ");
        System.out.println(one);

        System.out.print("Created itemFloat: ");
        System.out.println(floatOne);

        System.out.print("Created itemFloat: ");
        System.out.println(floattwo);

        System.out.print("Created itemFloat: ");
        System.out.println(floatthre);

        System.out.print("Created itemFloat: ");
        System.out.println(floatfour);

        itemCacheRepo.save(one);
        itemFloatCacheRepo.save(floatOne);
        itemFloatCacheRepo.save(floattwo);
        itemFloatCacheRepo.save(floatthre);
        itemFloatCacheRepo.save(floatfour);

        System.out.println("////////////////////////////");

        for(ItemFloat i : itemFloatCacheRepo.getItems()){
            System.out.println(i);
        }

        System.out.println("////////////////////////////");

        itemFloatCacheRepo.filter((ItemFloat i) -> {
            {
                if (i.getQuantity() > 20) {
                    return true;
                } else {
                    return false;
                }

            }
        }).forEach(System.out::println);



    }
}
