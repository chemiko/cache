package com.company.items;

/**
 * Created by me on 17.03.2016.
 */
public interface IEntity<T> {

    void setId(T id);
    T getId();
}
