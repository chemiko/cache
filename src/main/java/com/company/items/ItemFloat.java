package com.company.items;


public class ItemFloat implements IEntity<Float> {
    private static Float lastId = 0.275F;
    private Float _id;

    private String name;
    private double price;
    private int quantity;

    public ItemFloat(){
        name = "null";
        price = 0.0;
        quantity = 0;
        _id = lastId + 1;
    }

    public ItemFloat(String itemName, double itemPrice, int itemQuantity){
        name = itemName;
        price = itemPrice;
        quantity = itemQuantity;
        _id = lastId++;
    }

    @Override
    public void setId(Float id) {
        _id = id;
    }

    @Override
    public Float getId() {
        return _id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public String toString(){
        return "\tid: " + _id + ", \tname: " + name + ", \tprice: " +  price + ", quantity: " + quantity;
    }


}
