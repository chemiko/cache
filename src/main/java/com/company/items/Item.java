package com.company.items;


public class Item implements IEntity<Long> {
    private static long lastId = 0;
    private long _id;

    private String name;
    private double price;
    private int quantity;

    public Item(){
        name = "null";
        price = 0.0;
        quantity = 0;
        _id = lastId++;
    }

    public Item(String itemName,double itemPrice, int itemQuantity){
        name = itemName;
        price = itemPrice;
        quantity = itemQuantity;
        _id = lastId++;
    }

    @Override
    public void setId(Long id) {
        _id = id;
    }

    @Override
    public Long getId() {
        return _id;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public String toString(){
        return " id: " + _id + ", name: " +  name + ", price: "  + price + ", quantity: " + quantity;
    }


}
