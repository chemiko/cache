package com.company.data.db;

/**
 * Created by me on 29.03.2016.
 */
public class Position {
    private Long id;
    private String name;

    public Position(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String toString(){
        StringBuffer b = new StringBuffer();
        b.append("id: ");
        b.append(id);
        b.append(", name: ");
        b.append(name);
        return b.toString();
    }
}
