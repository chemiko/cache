package com.company.data.db;

import java.util.Date;

/**
 * Created by me on 29.03.2016.
 */

public class Person {
    private Long id;
    private String firstname;
    private String lastname;
    private Date birtdate;

    public Person(Long id, String firstname, String lastname, Date birtdate) {
        this.id = id;
        this.firstname = firstname;
        this.lastname = lastname;
        this.birtdate = birtdate;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public Date getBirtdate() {
        return birtdate;
    }

    public void setBirtdate(Date birtdate) {
        this.birtdate = birtdate;
    }

    public String toString(){
        StringBuffer b = new StringBuffer();
        b.append("id: ");
        b.append(id);
        b.append(" firstname: ");
        b.append(firstname);
        b.append(" lastname: ");
        b.append(lastname);
        b.append(" birthday: ");
        b.append(birtdate);

        return b.toString();

    }


}
