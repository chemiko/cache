package com.company.data.db;

import java.util.List;

/**
 * Created by me on 29.03.2016.
 */
public class DBMain {
    public static void main(String[] args){
        //SimpleDBOperator.inputAnyPersons();
        List<Person> persons = SimpleDBOperator.findAllPerson();
        for(Person e : persons){
            System.out.println(e);
        }
        List<Position> positions = SimpleDBOperator.findAllPositions();
        for(Position e : positions){
            System.out.println(e);
        }
        SimpleDBOperator.createAnEmployee(persons.get(2), positions.get(1),1000L);
        List<Employee> employees = SimpleDBOperator.findAllEmployee();
        for(Employee e : employees){
            System.out.println(e);
        }


    }
}
