package com.company.data.db;

import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

/**
 * Created by me on 29.03.2016.
 */
public class SimpleDBOperator {
    public static void inputAnyPersons(){
        try(Connection conn = ConnectionProvider.providePostgreConnection(false)){
            try(Statement statement = conn.createStatement()){
                statement.addBatch("INSERT INTO public.person (lastname,firstname,birthday) VALUES ('Василий', 'Пупкович',to_date('05.03.2000','DD.MM.YYYY'))");
                statement.addBatch("INSERT INTO public.person (lastname,firstname,birthday) VALUES ('Вас', 'Пуп',to_date('05.08.2000','DD.MM.YYYY'))");
                statement.addBatch("INSERT INTO public.person (lastname,firstname,birthday) VALUES ('Васили', 'Пупкови',to_date('05.09.2000','DD.MM.YYYY'))");
                int[] i = statement.executeBatch();
                for(int count : i){
                    System.out.println(count + "was inserted");
                }
                conn.commit();
            }catch (SQLException e){
                e.printStackTrace();
                conn.rollback();
            }
        }catch (IOException | SQLException e) {
            e.printStackTrace();
        }

    }


    public static List<Person> findAllPerson(){
        List<Person> persons = new ArrayList<>();
        try(Connection conn = ConnectionProvider.providePostgreConnection(false)){
            try(Statement statement = conn.createStatement()) {
                ResultSet rs = statement.executeQuery("SELECT * FROM public.person");
                while (rs.next()){
                    Long id = rs.getLong(1);
                    String lastname = rs.getString(2);
                    String firstname = rs.getString(3);
                    Date birthday = rs.getDate(4);
                    persons.add(new Person(id,lastname,firstname,birthday));

                }
            }
        }catch (IOException | SQLException e) {
        e.printStackTrace();
    }
        return persons;
    }

    public static List<Position> findAllPositions(){
        List<Position> positions = new ArrayList<>();
        try(Connection conn = ConnectionProvider.providePostgreConnection(false)){
            try(Statement statement = conn.createStatement()) {
                ResultSet rs = statement.executeQuery("SELECT * FROM public.position");
                while (rs.next()){
                    Long id = rs.getLong(1);
                    String name = rs.getString(2);
                    positions.add(new Position(id, name));
                }
            }
        }catch (IOException | SQLException e) {
            e.printStackTrace();
        }
        return positions;
    }

    public static void createAnEmployee(Person person, Position position, Long salary){

        try(Connection conn = ConnectionProvider.providePostgreConnection(false)){
            try(Statement statement = conn.createStatement()){
                StringBuilder b = new StringBuilder();
                b.append("INSERT INTO public.employ (position_id, person_id, salary) VALUES (");
                b.append(position.getId());
                b.append(", ");
                b.append(person.getId());
                b.append(", ");
                b.append(salary);
                b.append(")");

                statement.addBatch(b.toString());

                int[] i = statement.executeBatch();
                for(int count : i){
                    System.out.println(count + "was inserted");
                }
                conn.commit();
            }catch (SQLException e){
                e.printStackTrace();
                conn.rollback();
            }
        }catch (IOException | SQLException e) {
            e.printStackTrace();
        }

    }


    public static List<Employee> findAllEmployee(){
        List<Employee> employees = new ArrayList<>();
        List<Person> persons = findAllPerson();
        List<Position> positions = findAllPositions();

        try(Connection conn = ConnectionProvider.providePostgreConnection(false)){
            try(Statement statement = conn.createStatement()) {
                ResultSet rs = statement.executeQuery("SELECT * FROM public.employ");
                while (rs.next()){
                    Long id = rs.getLong(1);
                    Long position_id = rs.getLong(2);
                    Long person_id = rs.getLong(3);
                    Long salary = rs.getLong(4);
                    Position pos = null;
                    for(int i = 0; i < positions.size(); i++){
                        if(positions.get(i).getId() == position_id){
                            pos = positions.get(i);
                            break;
                        }
                    }
                    Person pers = null;
                    for(int i = 0; i < persons.size(); i++){
                        if(persons.get(i).getId() == person_id){
                            pers = persons.get(i);
                            break;
                        }
                    }
                    employees.add(new Employee(id,pos,pers,salary ));
                }
            }
        }catch (IOException | SQLException e) {
            e.printStackTrace();
        }
        return employees;
    }

    public static Person getPerson(Long i){
        Person p = null;

        try(Connection conn = ConnectionProvider.providePostgreConnection(false)){
            try(Statement statement = conn.createStatement()) {
                StringBuffer b = new StringBuffer();
                b.append("SELECT * FROM public.employ WHERE id = ");
                b.append(i);
                ResultSet rs = statement.executeQuery(b.toString());
                while (rs.next()){
                    Long id = rs.getLong(1);
                    String lastname = rs.getString(2);
                    String firstname = rs.getString(3);
                    Date birthday = rs.getDate(4);
                    p = new Person(id,firstname,lastname,birthday);
                }
            }
        }catch (IOException | SQLException e) {
            e.printStackTrace();
        }

        return p;
    }


}
