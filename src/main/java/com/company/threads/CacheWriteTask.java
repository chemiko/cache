package com.company.threads;

import com.company.IRepo;
import com.company.cached.ItemCacheRepo;
import com.company.cached.ItemFloatCacheRepo;
import com.company.items.IEntity;
import com.company.items.Item;
import com.company.items.ItemFloat;

import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by me on 04.04.2016.
 */
public class CacheWriteTask implements Runnable {

    private Random rand;
    private Long timeToWait;
    private IRepo<Long,Item> longRepo;
    private IRepo<Float,ItemFloat> floatRepo;

    public CacheWriteTask(Long timeToWait) {
        rand= ThreadLocalRandom.current();
        this.timeToWait = timeToWait;
        longRepo = new ItemCacheRepo();
        floatRepo = new ItemFloatCacheRepo();
    }

    @Override
    public void run() {

        try{
                while(!Thread.currentThread().isInterrupted()){
                    //System.out.print("write: ");
                    if(rand.nextBoolean()){
                        longRepo.save(new Item("item " + rand.nextInt(1000), rand.nextDouble(),rand.nextInt(1000)));
                    }else{
                        floatRepo.save(new ItemFloat("float item " + rand.nextInt(1000), rand.nextDouble(),rand.nextInt(1000)));
                    }



                    Thread.sleep(timeToWait);
                }
        }catch (InterruptedException e){
            e.printStackTrace();
        }


    }
}

