package com.company.threads;

import com.company.IRepo;
import com.company.cached.ItemCacheRepo;
import com.company.cached.ItemFloatCacheRepo;
import com.company.items.Item;
import com.company.items.ItemFloat;

import java.util.List;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by me on 04.04.2016.
 */
public class CacheRemoveTask implements Runnable {
    private Random rand;
    private Long timeToWait;
    private IRepo<Long,Item> longRepo;
    private IRepo<Float,ItemFloat> floatRepo;

    public CacheRemoveTask(Long timeToWait) {
        rand = ThreadLocalRandom.current();
        this.timeToWait = timeToWait;
        longRepo = new ItemCacheRepo();
        floatRepo = new ItemFloatCacheRepo();
    }

    @Override
    public void run() {
        try{
            while(!Thread.currentThread().isInterrupted()){

                if(rand.nextBoolean()){
                    List<Item> list = longRepo.getItems();
                    if(list.size() > 1){
                        //System.out.print("delete: ");
                        Long id = list.get(rand.nextInt(list.size() - 1)).getId();
                        longRepo.removeById(id);
                    }
                }else{
                    List<ItemFloat> list = floatRepo.getItems();
                    if(list.size() > 1) {
                        //System.out.print("read: ");
                        Float id = list.get(rand.nextInt(list.size() - 1)).getId();
                        floatRepo.removeById(id);
                    }
                }

                Thread.sleep(timeToWait);
            }
        }catch (InterruptedException e){
            e.printStackTrace();
        }

    }
}
