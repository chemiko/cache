package com.company;

import com.company.cached.Cache;
import com.company.items.IEntity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;

/**
 * Created by me on 17.03.2016.
 */
public class ItemIRepo implements IRepo<Long,IEntity<Long>> {
    private Map<Long,IEntity<Long>> repo;
    Cache cache = null;

    public ItemIRepo(){
        Cache cache = Cache.getInstance();
        //repo = cache.getRepo(new Long(1));
    }


    @Override
    public IEntity<Long> save(IEntity<Long> item) {
        repo.put(item.getId(),item);
        return item;
    }

    @Override
    public IEntity<Long> removeById(Long id) {
        if(repo.containsKey(id)){
            IEntity<Long> entity = repo.remove(id);
            return entity;
        }
        return null;
    }

    @Override
    public IEntity<Long> remove(IEntity<Long> item){
        return removeById(item.getId());
    }

    @Override
    public List<IEntity<Long>> getItems() {
        List<IEntity<Long>> coll = (List<IEntity<Long>>) repo.values();
        return coll;
    }

    @Override
    public IEntity<Long> getByID(Long id) {
        if(repo.containsKey(id)){
            IEntity<Long> entity = repo.get(id);
            return entity;
        }
        return null;
    }

    @Override
    public List<IEntity<Long>> filter(Predicate<IEntity<Long>> predicate) {

        List<IEntity<Long>> col = new ArrayList<>();
        for(IEntity<Long> e : repo.values()){
            if(predicate.test(e)){
                col.add(e);
            }
        }
        if(col.isEmpty()){
            return null;
        }
        return col;
    }
}
